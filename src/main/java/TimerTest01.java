
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TimerTest01 {
    private ScheduledExecutorService scheduExec;


    public long start;

    TimerTest01() {
        this.scheduExec = Executors.newScheduledThreadPool(10);
        this.start = System.currentTimeMillis();
    }

    public void timerTwo() {
        scheduExec.scheduleAtFixedRate(new Runnable() {
            public void run() {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
                System.out.println("开始执行:日期" + dateFormat.format(new Date()));
                test.sftpClon();
            }
        }, 0, 2, TimeUnit.MINUTES);
    }

    public static void main(String[] args) {
        TimerTest01 test = new TimerTest01();
        test.timerTwo();

    }


}